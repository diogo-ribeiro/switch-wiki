export default {
  colors: {
    primary: "#931a25",
    background: "#FFFFFF",
    shape: `#e97171`,
    title: `#e97171`,
    text: `#1F1715`,
  },
};
